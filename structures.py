class Node:

    def __init__(self, node_type, left, right, source_sink_edge):
        self.node_type = node_type
        self.left = left
        self.right = right
        self.source_sink_edge = source_sink_edge

class Tree:

    def __init__(self, i, l, sigma, tau):
        self.i = i
        self.l = l
        self.sigma = sigma
        self.tau = tau

    def __eq__(self, other):
        size_equality = self.i == other.i
        leaves_equality = self.l == other.l
        sigma_equality = self.sigma == other.sigma
        tau_equality = self.tau == other.tau
        return size_equality and leaves_equality and sigma_equality and tau_equalityi

class Forest:

    def __init__(self, i, l, sigma, tau):
        self.i = i
        self.l = l
        self.sigma = sigma
        self.tau = tau

    def __eq__(self, other):
        size_equality = self.i == other.i
        leaves_equality = self.l == other.l
        sigma_equality = self.sigma == other.sigma
        tau_equality = self.tau == other.tau
        return size_equality and leaves_equality and sigma_equality and tau_equality