from structures import Node, Tree, Forest
from auxiliary_functions import *

near = [0, 1, 2]
far = [3, 4]

def compute_ist_source(root):
    current = {}
    if root.node_type == 'leaf':
        current[1] = [Tree(1, 0, 0, 3)]
        current[2] = [Tree(2, 2, 1, 1)]
    elif root.node_type == 'serie':
        sL = compute_ist_source(root.left)
        stL = compute_ist_source_sink(root.left)
        sR = compute_ist_source(root.right)
        for left in sL.values():
            if root.right.source_sink_edge and left.tau in near:
                tau = 3
            else:
                tau = 4
            addTree(current, Tree(left.i, left.l, left.sigma, tau)
        for left in stL.values():
            for right in sR.values():
                i = left.i + right.i - 1
                l = left.l + right.l - rho(left.tau, right.sigma)
                addTree(current, Tree(i, l, left.sigma, right.tau)
    else:
        sL = compute_ist_source(root.left)
        sR = compute_ist_source(root.right)
        for left in sL.values():
            for right in sR.values():
                if distV(left.tau, right.tau):
                    i = left.i + right.i - 1
                    l = left.l + right.l - rho(left.sigma, right.sigma)
                    if left.sigma == 0:
                        sigma = right.sigma
                    elif right.sigma == 0:
                        sigma = left.sigma
                    else:
                        sigma = 2
                    tau = min([left.tau, right.tau])
                    addTree(current, Tree(i, l, sigma, tau)
    return current

def compute_ist_sink(root):
    current = {}
    if root.node_type == 'leaf':
        current[1] = [Tree(1, 0, 3, 0)]
        current[2] = [Tree(2, 2, 1, 1)]
    elif root.node_type == 'serie':
        tL = compute_ist_sink(root.left)
        stR = compute_ist_source_sink(root.right)
        tR = compute_ist_sink(root.right)
        for right in tR.values():
            if root.left.source_sink_edge and right.sigma in near:
                sigma = 3
            else:
                sigma = 4
            addTree(current, Tree(right.i, right.l, sigma, right.tau)
        for left in tL.values():
            for right in stR.values():
                i = left.i + right.i - 1
                l = left.l + right.l - rho(left.tau, right.sigma)
                addTree(current, Tree(i, l, left.sigma, right.tau)
    else:
        tL = compute_ist_sink(root.left)
        tR = compute_ist_sink(root.right)
        for left in tL.values():
            for right in tR.values():
                if distV(left.sigma, right.sigma):
                    i = left.i + right.i - 1
                    l = left.l + right.l - rho(left.tau, right.tau)
                    if left.tau == 0:
                        tau = right.tau
                    elif right.tau == 0:
                        tau = left.tau
                    else:
                        tau = 2
                    sigma = min([left.sigma, right.sigma])
                    addTree(current, Tree(i, l, sigma, tau))
    return current

def compose(current, st, s, t, forest, condition):
    for left in st.values():
        for right in s.vlaues():
            if distV(left.tau, right.tau):
                i = left.i + right.i - 1
                l = left.l + right.l - rho(left.sigma, right.sigma)
                if right.sigma == 0:
                    sigma = left.sigma
                else:
                    sigma = 2
                addTree(current, Tree(i, l, sigma, left.tau))
        for right in t.values():
            if distV(left.sigma, right.sigma):
                i = left.i + right.i - 1
                l = left.l + right.l - rho(left.tau, right.tau)
                if right.tau == 0:
                    tau = left.tau
                else:
                    tau = 2
                addTree(current, Tree(i, l, left.sigma, tau))
        for f in forest.values():
            i = left.i + right.i - 2
            l = left.l + right.l - rho(left.sigma, right.sigma) - rho(left.tau, right.tau)
            if right.sigma == 0:
                sigma = left.sigma
            else:
                sigma = 2
            if right.tau == 0:
                tau = left.tau
            else:
                tau = 2
            addTree(current, Tree(i, l, sigma, tau))
        if not condition:
            addTree(current, Tree(left.i, left.l, left.sigma, left.tau))

def compute_ist_source_sink(root):
    current = {}
    if root.node_type == 'leaf':
        current[2] = [Tree(2, 2, 1, 1)]
    elif root.node_type == 'serie':
        stL = compute_ist_source_sink(root.left)
        stR = compute_ist_source_sink(root.right)
        for left in stL.values():
            for right in stR.values():
                i = left.i + right.i - 1
                l = left.l + right.l - rho(left.tau, right.sigma)
                addTree(current, Tree(i, l, left.sigma, right.tau))
    else:
        stL = compute_ist_source_sink(root.left)
        sR = compute_ist_source(root.right)
        tR = compute_ist_sink(root.right)
        fR = compute_forests(root.right)
        compose(current, stL, sR, tR, fR, root.right.source_sink_edge)
        stR = compute_ist_source_sink(root.right)
        sL = compute_ist_source(root.left)
        tL = compute_ist_sink(root.left)
        fL = compute_forests(root.left)
        compose(current, stR, sL, tL, fL, root.left.source_sink_edge)
    return current