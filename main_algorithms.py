from structures import Node, Tree, Forest
from auxiliary_functions import *
from intermediate_algorithms import *

near = [0, 1, 2]
far = [3, 4]

def compute_forests(root):
    current = {}
    if root.node_type == 'serie':
        sL = compute_ist_source(root.left)
        tR = compute_ist_sink(root.right)
        for left in sL.values():
            for right in tR.values():
                if distV(left.tau, right.sigma):
                    i = left.i + right.i
                    l = left.l + right.l
                    addForest(current, Forest(i, l, left.sigma, right.tau))
        stL = compute_ist_source_sink(root.left)
        fR = compute_forests(root.right)
        for left in stL.values():
            for f in fR.values():
                i = left.i + f.i - 1
                l = left.l + f.l - rho(left.tau, f.sigma)
                addForest(current, Forest(i, l, left.sigma, f.tau))
        stR = compute_ist_source_sink(root.right)
        fL = compute_forests(root.left)
        for right in stR.values():
            for f in fL.values():
                i = right.i + f.i - 1
                l = right.l + f.l - rho(right.sigma, f.tau)
                addForest(current, Forest(i, l, f.sigma, right.tau))
    elif root.node_type == 'parallel':
        fL = compute_forests(root.left)
        fR = compute_forests(root.right)
        for left in fL.values():
            for right in fR.values():
                i = left.i + right.i - 2
                l = left.l + right.l - rho(left.sigma, right.sigma) - rho(left.tau, right.tau)
                if left.sigma != 0 and right.sigma != 0:
                    sigma = 2
                else:
                    sigma = max([left.sigma, right.sigma])
                if left.tau != 0 and right.tau != 0:
                    tau = 2
                else:
                    tau = max([left.tau, right.tau])
                addForest(current, Forest(i, l, sigma, tau))
    return current

def pa1_pa2(current, flis, condition):
    for a in flis.values():
        if condition:
            if a.sigma in far or a.tau in far:
                if a.tau in near:
                    sigma = 3
                else:
                    sigma = a.sigma
                if a.sigma in near:
                    tau = 3
                else:
                    tau = a.tau
                addTree(current, Tree(a.i, a.l, sigma, tau))
        else:
            addTree(current, Tree(a.i, a.l, a.sigma, a.tau))

def pa5_pa6(current, ST, F):
    for a in SP.values():
        for f in F.values():
            i = a.i + f.i - 2
            l = a.l + f.l - - rho(a.sigma, f.sigma) - rho(a.tau, f.tau)
            if f.sigma == 0:
                sigma = a.sigma
            else:
                sigma = 2
            if f.tau == 0:
                tau = a.tau
            else:
                tau = 2
            addTree(current, Tree(i, l, sigma, tau))

def compute_flis(root):
    current = {}
    if root.node_type == 'leaf':
        current[1] = [Tree(1, 0, 3, 0), Tree(1, 0, 0, 3)]
        current[2] = [Tree(2, 2, 1, 1)]
    elif root.node_type == 'serie':
        flisL = compute_flis(root.left)
        flisR = compute_flis(root.right)
        tL = compute_ist_sink(root.left)
        sR = compute_ist_source(root.right)
        for a in flisL.values():
            if root.right.source_sink_edge and a.tau in near:
                tau = 3
            else:
                tau = 4
            addTree(current, Tree(a.i, a.l, a.sigma, tau))
        for a in flisR.values():
            if root.left.source_sink_edge and a.sigma in near:
                sigma = 3
            else:
                sigma = 4
            addTree(current, Tree(a.i, a.l, sigma, a.tau))
        for left in tL.values():
            for right in sR.values():
                i = left.i + right.i - 1
                l = left.l + right.l - rho(left.tau, right.sigma)
                addTree(current, Tree(i, l, left.sigma, right.tau))
    else:
        flisL = compute_flis(root.left)
        flisR = compute_flis(root.right)
        pa1_pa2(current, flisL, root.right.source_sink_edge)
        pa1_pa2(current, flisD, root.left.source_sink_edge)
        sL = compute_ist_source(root.left)
        sR = compute_ist_source(root.right)
        for left in sL.values():
            for right in sR.values():
                if distV(left.tau, right.tau):
                    i = left.i + right.i - 1
                    l = left.l + right.l - rho(left.sigma, right.sigma)
                    if left.sigma != and right.sigma != 0:
                        sigma = 2
                    else:
                        sigma = max([left.sigma, right.sigma])
                    tau = min([left.tau, right.tau])
                    addTree(current, Tree(i, l, sigma, tau))
        tL = compute_ist_sink(root.left)
        tR = compute_ist_sink(root.right)
        for left in tL.values():
            for right in tR.values():
                if distV(left.sigma, right.sigma):
                    i = left.i + right.i - 1
                    l = left.l + right.l - rho(left.tau, right.tau)
                    if left.tau != and right.tau != 0:
                        tau = 2
                    else:
                        tau = max([left.tau, right.tau])
                    sigma = min([left.sigma, right.sigma])
                    addTree(current, Tree(i, l, sigma, tau))
        stL = compute_ist_source_sink(root.left) 
        fL = compute_forests(root.left)
        stR = compute_ist_source_sink(root.right)
        fR = compute_forests(root.right)
        pa5_pa6(current, stL, fR)
        pa5_pa6(current, stR, fL)

def leaf_function(root):
    initializeSourceSinkEdge(root)
    flis = compute_flis(root)
    L = {}
    for a in flis.values():
        L[a.i] = a.l
    return L
