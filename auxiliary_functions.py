from structures import Node, Tree, Forest

def distV(a, b):
    far = [3, 4]
    near = [0, 1, 2]
    if (a in far and b in far) or (a in near and b == 4) or (a == 4 and b in near):
        return True
    else:
        return False

def rho(a, b):
    if a == 1 and b == 1:
        return 2
    elif (a == 1 and b == 2) or (a == 2 and b == 1):
        return 1
    else:
        return 0

def addTree(current, tree):
    i = tree.i
    l = tree.l
    if i in current:
        trees_list = current[i]
        rep_tree = trees_list[0]
        if l == rep_tree.l and tree not in trees_list:
            trees_list.append(tree)
        elif l > rep_tree.l:
            trees_list = [tree]
        current[i] = trees_list
    else:
        curent[i] = [tree]

def addForest(current, forest):
    i = forest.i
    l = forest.l
    if i in current:
        forests_list = current[i]
        rep_forest = forests_list[0]
        if l == rep_forest.l and forest not in forests_list:
            forests_list.append(forest)
        elif l > rep_tree.l:
            forests_list = [forest]
        current[i] = forests_list
    else:
        curent[i] = [forest]

def initializeSourceSinkEdge(root):
    if root.node_type == 'leaf':
        root.source_sink_edge = True
    else:
        initializeSourceSinkEdge(root.left)        
        initializeSourceSinkEdge(root.right)        
        if root.node_type == 'parallel':
            root.source_sink_edge = root.left.source_sink_edge or root.right.source_sink_edge
        else:
            root.source_sink_edge = False